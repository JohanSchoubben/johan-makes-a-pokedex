
let path1 = "https://pokeapi.co/api/v2/pokemon/";
let htmlId = document.getElementById("pokemon-data");
let id = 0;
async function getData(id) {
    for (let index = 1; index <= id; index++) {
        await fetchData(index);
    }
}

async function fetchData(id) {
    let url = `${path1}${id}`;
    let quantity = document.getElementById("counter").innerHTML = `Quantity = ${id}`;
    let response = await fetch(url);
    let data = await response.json();
    console.log(data);
    // console.log(Object.getOwnPropertyNames(data));
    // return data;
    createCard(data);
}


function createCard(data) {
    let elem = document.createElement("div");
    elem.classList.add("pokemon");
    // let pokemonInnerHtml = `${data['forms'][0]['name']}`;
    // let pokemonInnerHtml = `${data.forms[0].name}`;

    let { id, name, sprites, types } = data;
    let type = types[0].type.name;
    let pokemonInnerHtml = `
    <div class="images">
        <img src="${sprites.front_default}"/>
    </div>
    <div class="details">
        <span class="number">${id}</span>
        <h3 class="name">${name}</h3>
        <span class="type">${type}</span>
    </div>
    `;
    elem.innerHTML = pokemonInnerHtml;
    htmlId.appendChild(elem);
}
getData(50);