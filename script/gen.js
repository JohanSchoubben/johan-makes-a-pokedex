
let id = 0;
let pathGenerations = "https://pokeapi.co/api/v2/generation/";
let htmlGenerations = document.getElementById("generations");
let pathSpecies = "https://pokeapi.co/api/v2/pokemon-species/";
let htmlSpecies = document.getElementById("pokemon-species");

async function fetchGeneration(id) {
    let url = `${pathGenerations}${id}`;
    let response = await fetch(url);
    let result = await response.json();
    createGeneration(result);
}
async function getGeneration(id) {
    for (let index = 1; index <= id; index++) {
        await fetchGeneration(index);
    }
}
function createGeneration(result) {
    let option = document.createElement("option");
    option.classList.add("generation");
    let generation = `Generation ${result.id} - ${result.main_region.name}`;
    let generationInnerHtml = `
        <option class="generation"> ${generation}</option>
    `;
    option.innerHTML = generationInnerHtml;
    htmlGenerations.appendChild(option);
}

getGeneration(8);

async function fetchSpecies(id) {
    let url = `${pathSpecies}${id}`;
    let response = await fetch(url);
    let species = await response.json();
    console.log(species.capture_rate);
    console.log(species.color.name);
    console.log(species.flavor_text_entries[0].flavor_text);
    console.log(species.habitat.name);
    createSpecies(species);

}
async function createSpecies(species) {
    let div = document.createElement("div");
    div.classList.add("species");
    let spec = `Flavor text: ${species.flavor_text_entries[6].flavor_text}`;
    let htmlSpeciesInnerHTML = `${spec}`;
    console.log(spec);
    div.innerHTML = htmlSpeciesInnerHTML;
    htmlSpecies.appendChild(div);
}
fetchSpecies(5);